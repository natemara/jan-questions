(async () => {
  const spinnerStyle = `
        .lds-dual-ring {
            display: inline-block;
            width: 20px;
            height: 20px;
        }
        .lds-dual-ring:after {
            content: " ";
            display: block;
            width: 12px;;
            height: 12px;;
            margin: 8px;
            border-radius: 50%;
            border: 6px solid #000;
            border-color: #000 transparent #000 transparent;
            animation: lds-dual-ring 1.2s linear infinite;
        }
        @keyframes lds-dual-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
        
        .answergrading-Right {
            background-color: rgba(20, 255, 20, 100);
        }

        .answergrading-Wrong {
            background-color: rgba(255, 20, 20, 100);
        }
        `;

  const styleElt = document.createElement('style');
  styleElt.type = 'text/css';
  styleElt.innerText = spinnerStyle;
  document.querySelector('head').appendChild(styleElt);

  const title = document.querySelector('.ilc_page_title_PageTitle');
  const statusElt = document.createElement('div');
  statusElt.classList.add('lds-dual-ring');
  title.appendChild(statusElt);

  const titleSplit = title.innerText.split(' ');
  const questionNumber = titleSplit[titleSplit.length - 1];

  const form = $('#taForm');
  const formData = form.serialize() + '&cmd[showInstantResponse]=Check';
  const formURL = form.attr('action');

  const response = await fetch(formURL, {
    credentials: 'same-origin',
    method: 'POST',
    redirect: 'follow',
    body: formData,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  });

  const pageText = await response.text();

  title.removeChild(statusElt);

  const parser = new DOMParser();
  const htmlDoc = parser.parseFromString(pageText, 'text/html');

  const answerBank = htmlDoc.querySelectorAll('.ilc_question_Standard')[1];

  const correctedChoices = {};
  for (let answer of answerBank.querySelectorAll('.ilc_qanswer_Answer')) {
    const img = answer.querySelector('img').getAttribute('alt');
    const correct = img.toLowerCase().trim() === 'checked';

    const text = answer.querySelector('.answertext').innerText;

    correctedChoices[text] = correct;
  }

  const checkAnswers = document.querySelectorAll('.ilc_qanswer_Answer');
  let correct = true;
  for (let answer of checkAnswers) {
    const checked = answer.querySelector('input').checked;
    const text = answer.querySelector('.answertext').innerText;

    if (correctedChoices[text] != checked) {
      correct = false;
    }
  }

  if (correct) {
    const checkMark = document.createElement('span');
    checkMark.innerText = '✅';

    title.appendChild(checkMark);

    setTimeout(() => {
      const next = document.querySelector('#bottomnextbutton');
      next.click();
    }, 2000);
  } else {
    for (let answer of checkAnswers) {
      const text = answer.querySelector('.answertext').innerText;
      let className = 'Wrong';

      if (correctedChoices[text]) {
        className = 'Right';
      }

      answer.classList.add(`answergrading-${className}`);
    }
  }

  const gradingString = localStorage.getItem('grading') || '{}';
  const grading = JSON.parse(gradingString);

  grading[questionNumber] = correct;

  localStorage.setItem('grading', JSON.stringify(grading));
})();
