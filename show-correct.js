(() => {
  const gradingString = localStorage.getItem('grading') || '{}';
  const grading = JSON.parse(gradingString);

  const list = document.querySelector('.shortlist');

  const questions = list.querySelectorAll('.ilTstNavElem');

  for (let elt of questions) {
    const split = elt.innerText.split(' ');
    const number = split[split.length - 1];
    if (grading[number] !== undefined) {
      let sym = '❌';
      if (grading[number]) {
        sym = '✅';
      }

      elt.innerText += sym;
    }
  }
})();
